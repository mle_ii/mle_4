from pyspark.ml.feature import PCA
from pyspark.ml.linalg import Vectors
import numpy as nm
import warnings

import numpy as np
import pandas as pd
import pandas as pd
from pyspark.sql import SparkSession
import glob
from pyspark.ml.clustering import KMeans
from pyspark.ml.evaluation import ClusteringEvaluator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.feature import StandardScaler
from pyspark.sql.functions import col, udf
from pyspark.ml.functions import vector_to_array
from pyspark.ml import Pipeline, Transformer
from pyspark.ml.classification import RandomForestClassifier
from pyspark.ml.evaluation import MulticlassClassificationEvaluator
from sklearn.metrics import classification_report, confusion_matrix
from pyspark.ml.classification import LogisticRegression, OneVsRest
from pyspark.sql.types import FloatType, ArrayType, DoubleType
import pyspark.sql.functions as F
from pyspark.ml.functions import array_to_vector
import scipy.stats as sst





class ColumnDropper(Transformer):
    """
    Drops mentioned colimns
    """

    def __init__(self, todrop):
        super(ColumnDropper, self).__init__()
        self.todrop = todrop

    def _transform(self, df):
        df = df.drop(*[x for x in df.columns if any(y in x for y in self.todrop)])
        return df







warnings.filterwarnings("ignore")



spark = (SparkSession.builder.appName("Datacreate").getOrCreate())

filename = glob.glob("data_with_lables/*.csv")

dat = (spark.read.format("csv").option("header", "true").option("delimiter", "\t").load(filename))




data = dat.withColumn('features', array_to_vector(F.from_json('features', "array<double>")))

from pyspark.sql.types import IntegerType
data = data.withColumn("prediction", data["prediction"].cast(IntegerType()))

data


data.show()
data.printSchema()

databackup = data.selectExpr("features as features_my", "prediction as prediction_my", "code as code", "product_name as product_name" )

data = data.selectExpr("features as features_my", "prediction as prediction_my")



cd = ColumnDropper(todrop = ["rawPrediction", "probability", ])




rf = RandomForestClassifier(labelCol="prediction_my", featuresCol="features_my", predictionCol='class_preds', numTrees=10)

lr = LogisticRegression(maxIter=10, tol=1E-6, fitIntercept=True, labelCol="prediction_my", featuresCol="features_my",
                        probabilityCol="lr_prob")

pipeline = Pipeline(stages=[rf, cd, lr])




# Fit the pipeline to training documents.
model = pipeline.fit(data)
output = model.transform(data)
print("output from data")
output.show()








clust_evaluator = ClusteringEvaluator(predictionCol='prediction_my', featuresCol='features_my', metricName='silhouette',
                                      distanceMeasure='squaredEuclidean')

class_evaluator = MulticlassClassificationEvaluator(labelCol="prediction_my", predictionCol="class_preds",
                                                    metricName="accuracy")

accuracy = class_evaluator.evaluate(output)
score = clust_evaluator.evaluate(output)


y_true = np.array(output.select("prediction_my").collect())
y_pred = np.array(output.select("class_preds").collect())


print(f"Score is: (best is 1 worst is -1) = = {score}")
print(f"Classification accuracy: {accuracy}")
print(f"Classification report: \n {classification_report(y_true, y_pred, zero_division=1)}")
print(f"Classification confusion matrix: \n {confusion_matrix(y_true, y_pred)}")
output = output.withColumn("class_preds", col("class_preds").cast("integer"))
output = output.withColumn("lr_prob", vector_to_array("lr_prob"))
prb = udf(lambda x, y: x[y], returnType=DoubleType())
output = output.withColumn('lr_estim_rf', prb(output['lr_prob'], output['class_preds']))
print()
print("LR estimation of RF labels:")

output.printSchema()
print(np.array(output.select("lr_estim_rf").collect()).squeeze())

output.select(["prediction_my", "class_preds", "lr_estim_rf"]).show(n=5)



result = output.select(["features_my","prediction_my", "class_preds", "lr_estim_rf"])

#res = result.join(result, result.features_my == data.features_my)
res = result.join(databackup, 
databackup.features_my==result.features_my).drop(result.features_my).drop(result.prediction_my)
    
res.show()

